import React from 'react'
import Head from 'next/head'
import dynamic from 'next/dynamic'

const Screen = dynamic(() => import('../screens/screen--coach1-client'), {
  ssr: false,
})

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Chat (Coach1 - Client)</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Screen />
      </main>

      <style jsx>{``}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  )
}
