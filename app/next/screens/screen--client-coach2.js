import React, {useCallback, useEffect, useState} from 'react'
import PubNub from 'pubnub'
import {PubNubProvider, usePubNub} from 'pubnub-react'
// import './App.css'

const pubnub = new PubNub({
  publishKey: 'pub-c-2aee29ca-1978-4c11-8f17-6e2df4725d6f',
  subscribeKey: 'sub-c-3a4e4112-cc52-11ea-b0f5-2a188b98e439',
})

const defaults = {
  userId: `client`,
  channels: ['coach2-client'],
}

const Chat = () => {
  const pubnub = usePubNub()
  const [messages, setMessages] = useState([])
  const [input, setInput] = useState('')

  useEffect(() => {
    pubnub.addListener({
      message: (messageEvent) => {
        setMessages([...messages, messageEvent])
      },
    })

    pubnub.subscribe({channels: defaults.channels})
  }, [messages])

  const sendMessage = useCallback(
    async (message) => {
      await pubnub.publish({
        channel: defaults.channels[0],
        message,
        meta: {
          from: {
            userId: defaults.userId,
          },
        },
      })

      setInput('')
    },
    [pubnub, setInput],
  )

  return (
    <div className="App">
      <header className="App-header">
        <div
          style={{
            width: '500px',
            height: '300px',
            border: '1px solid black',
          }}
        >
          <div style={{backgroundColor: 'grey'}}>React Chat Example</div>
          <div
            style={{
              backgroundColor: 'white',
              height: '260px',
              overflow: 'scroll',
            }}
          >
            {messages.map((message, messageIndex) => {
              return (
                <div
                  key={`message-${messageIndex}`}
                  style={{
                    display: 'inline-block',
                    float: 'left',
                    backgroundColor:
                      message?.userMetadata?.from?.userId === defaults.userId
                        ? '#eee'
                        : `#fee`,
                    color: 'black',
                    borderRadius: '20px',
                    margin: '5px',
                    padding: '8px 15px',
                  }}
                >
                  {message.message}
                </div>
              )
            })}
          </div>
          <div
            style={{
              display: 'flex',
              height: '40px',
              backgroundColor: 'lightgrey',
            }}
          >
            <input
              type="text"
              style={{
                borderRadius: '5px',
                flexGrow: 1,
                fontSize: '18px',
              }}
              placeholder="Type your message"
              value={input}
              onChange={(e) => setInput(e.target.value)}
            />
            <button
              style={{
                backgroundColor: 'blue',
                color: 'white',
                borderRadius: '5px',
                fontSize: '16px',
              }}
              onClick={(e) => {
                e.preventDefault()
                sendMessage(input)
              }}
            >
              Send Message
            </button>
          </div>
        </div>
      </header>
    </div>
  )
}

export default function ScreenClient2Coach() {
  return (
    <PubNubProvider client={pubnub}>
      <Chat />
    </PubNubProvider>
  )
}
